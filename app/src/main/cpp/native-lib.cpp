#include <jni.h>
#include <string>

using namespace std;

string WX_ID = "your wechat id";
string WX_KEY = "your wechat key";
string QQ_ID = "your qq id";
string QQ_KEY = "your qq key";

string SERVER_ADDRESS = "http://123.207.116.238/joke/";


/**
 * 获取微信APP_ID
 */
extern "C" JNIEXPORT jstring
JNICALL Java_com_xiaolei_openokjoke_JNI_JokeLib_getWXAPPID(JNIEnv *env, jobject /*this*/)
{
    return env->NewStringUTF(WX_ID.c_str());
}

/**
 * 获取微信APP_KEY
 */
extern "C" JNIEXPORT jstring
JNICALL Java_com_xiaolei_openokjoke_JNI_JokeLib_getWXAPPKEY(JNIEnv *env, jobject /*this*/)
{
    return env->NewStringUTF(WX_KEY.c_str());
}

/**
 * 获取QQ的APP_ID 
 */
extern "C" JNIEXPORT jstring
JNICALL Java_com_xiaolei_openokjoke_JNI_JokeLib_getQQAPPID(JNIEnv *env, jobject /*this*/)
{
    return env->NewStringUTF(QQ_ID.c_str());
}

/**
 *  获取QQ的APP_KEY
 */
extern "C" JNIEXPORT jstring
JNICALL Java_com_xiaolei_openokjoke_JNI_JokeLib_getQQAPPKEY(JNIEnv *env, jobject /*this*/)
{
    return env->NewStringUTF(QQ_KEY.c_str());
}

/**
 * 获取服务器地址
 */
extern "C" JNIEXPORT jstring
JNICALL Java_com_xiaolei_openokjoke_JNI_JokeLib_getServerAddress(JNIEnv *env, jobject /*this*/)
{
    return env->NewStringUTF(SERVER_ADDRESS.c_str());
}