package com.xiaolei.openokjoke.Net

/**
 * 所有的请求uri都在这里，并且详细写上注释
 * Created by xiaolei on 2017/3/14.
 */
object UriContext
{
    const val joke = "joke"
    const val feedback = "feedback"
    const val qqlucky = "qqlucky"
    const val sharereport = "sharereport"
    const val sharemsg = "sharemsg"
    const val report = "report"
    const val aboutme = "aboutme"
}
