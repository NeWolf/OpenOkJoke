package com.xiaolei.openokjoke.Fragments

import android.content.Context
import android.os.Message
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.xiaolei.openokjoke.Activitys.FeedBackActivity
import com.xiaolei.openokjoke.Activitys.QQLuckyActivity
import com.xiaolei.openokjoke.Activitys.SettingActivity
import com.xiaolei.openokjoke.Base.BaseV4Fragment
import com.xiaolei.openokjoke.Configs.EventAction
import com.xiaolei.openokjoke.Configs.Globals
import com.xiaolei.openokjoke.Exts.edit
import com.xiaolei.openokjoke.R
import com.xiaolei.openokjoke.Utils.StateBarUtil
import kotlinx.android.synthetic.main.fragment_left_menu.*

/**
 * 侧边菜单栏
 * Created by xiaolei on 2018/3/12.
 */
class LeftMenuFragment : BaseV4Fragment()
{
    override fun contentViewId() = R.layout.fragment_left_menu

    override fun initObj()
    {

    }

    override fun initView()
    {
        Glide.with(this)
                .load(R.drawable.icon_menu_head)
                .into(head)
        val layout = head.layoutParams as LinearLayout.LayoutParams
        layout.topMargin += StateBarUtil.getStateBarHeigh(this)
        head.requestLayout()

        val sharedPreferences = activity?.getSharedPreferences(Globals.spfile, Context.MODE_PRIVATE)
        val autoLoadGif = sharedPreferences?.getBoolean("autoLoadGif", true)
        switch_button.isChecked = autoLoadGif ?: true
    }

    override fun initData()
    {

    }

    override fun setListener()
    {
        feedback_layout.setOnClickListener {
            post(Message.obtain().apply {
                what = EventAction.closeDrawer
            })
            startActivity(FeedBackActivity::class.java)
        }
        qq_lucky_testing.setOnClickListener {
            post(Message.obtain().apply {
                what = EventAction.closeDrawer
            })
            startActivity(QQLuckyActivity::class.java)
        }
        setting_layout.setOnClickListener {
            post(Message.obtain().apply {
                what = EventAction.closeDrawer
            })
            startActivity(SettingActivity::class.java)
        }
        switch_button.setOnCheckedChangeListener { _, isChecked ->
            val sp = activity?.getSharedPreferences(Globals.spfile, Context.MODE_PRIVATE)
            sp?.edit {
                putBoolean("autoLoadGif", isChecked)
            }
        }
    }

    override fun loadData()
    {

    }
}